﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundController : MonoBehaviour
{
    //Global variables
    [SerializeField] private Bird bird;
    [SerializeField] private float speed = 1;
    [SerializeField] private Transform StartPos;
    [SerializeField] private Transform EndPos;
    public GameObject groundObject;

    private void Start()
    {
        
    }

    //Dipanggil setiap frame
    private void Update()
    {
        //Melakukan pengecekan jika burung null atau belu mati
        if (bird == null || (bird != null && !bird.IsDead()))
        {
            //Membuat pipa bergerak kesebelah kiri dengan kecepatan dari variable speed
            transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
            if (transform.position.x <= EndPos.position.x)
            {
                transform.position = new Vector3(StartPos.position.x, 0,0);
            }
        }
    }

}
