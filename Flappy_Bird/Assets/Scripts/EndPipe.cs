﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPipe : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Membuat burung mati ketika bersentuhan dengan game object ini
        if (collision.gameObject.tag == "Pipe")
        {
            Debug.Log("called");
            Destroy(collision.gameObject);
        }
    }
}
